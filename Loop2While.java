import java.util.Scanner;


public class Loop2While {
    public static void main(String[] args) {
        int opcao = 0;
        while (opcao != 3 | opcao > 3) {

            System.out.println("|    Menu    |");
            System.out.println("|Opções:     |");
            System.out.println("|1. Opção 1  |");
            System.out.println("|2.Opção  2  |");
            System.out.println("|3.Sair.     |");
            Scanner menu = new Scanner(System.in);
            System.out.println("Escolha uma opção:");
            opcao = menu.nextInt();

            switch (opcao) {
                case 1:
                    System.out.println("Opção 1 Selecionada.");
                    break;
                case 2:
                    System.out.println("Opção 2 Selecionada.");
                    break;
                case 3:
                    System.out.println("Sair.");
                    break;
                default:
                    System.out.println("Opção Invalida");
            }

            }
        }
    }